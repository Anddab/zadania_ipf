import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;


public class Zadanie_2 {
	
	int maxValId;
	
	@Test
	public void maxUserIdValue()
	{

		RestAssured.baseURI = "https://jsonplaceholder.typicode.com/posts/";
		RequestSpecification request = RestAssured.given();
		Response response = request.get();

		Utils util = new Utils();
		int maxValUserId = util.maxValue(response, "userId");
		System.out.println("Najwyzsza wartosc dla UserId: " + maxValUserId);
	}
	
	@Test
	public void maxIdValue()
	{

		RestAssured.baseURI = "https://jsonplaceholder.typicode.com/posts/";
		RequestSpecification request = RestAssured.given();
		Response response = request.get();

		Utils util = new Utils();
		maxValId = util.maxValue(response, "id");
		System.out.println("Najwyzsza wartosc dla Id: " + maxValId);
	}

	@Test(dependsOnMethods = "maxIdValue")
	public void sendComment()
	{		
		RestAssured.baseURI ="https://jsonplaceholder.typicode.com";
		RequestSpecification request = RestAssured.given();

		JSONObject requestParams = new JSONObject();
		requestParams.put("postId", maxValId);
		requestParams.put("id", "1");
		requestParams.put("name", "Example Name");
		requestParams.put("email", "mail@mail.pl");
		requestParams.put("body", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.");
		request.body(requestParams.toString());
		Response response = request.post("/comments");
		int statusCode = response.getStatusCode();
		Assert.assertEquals(statusCode, 201);
		if(statusCode==201)
		{
			System.out.println("operacja zapisania JSONa wykonana poprawnie");
		}else
		{
			System.out.println("Podczas operacji nastapil blad");
		}
	}
}