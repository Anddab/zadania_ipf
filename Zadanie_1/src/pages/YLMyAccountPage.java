package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class YLMyAccountPage {

	WebDriver driver;

	@FindBy(xpath="//span[text()='Order history and details']")
	WebElement orderHistory;
	
	@FindBy(xpath="//div[@id='block-history']/p")
	WebElement infoPanel;
	
	
	@FindBy(xpath="//span[text()='My credit slips']")
	WebElement creditSlips;
	
	@FindBy(xpath="//span[text()='My addresses']")
	WebElement myAddress;
	
	@FindBy(xpath="//*[@id='center_column']/div[1]/p[1]/strong")
	WebElement addressesText;
	
	@FindBy(xpath="//span[text()='My personal information']")
	WebElement personalInfo;
	
	@FindBy(xpath="//h1[@class='page-subheading']")
	WebElement personalInfoHeader;

	@FindBy(xpath="//span[text()='My wishlists']")
	WebElement myWishList;
	
	@FindBy(xpath="//h1[@class='page-heading']")
	WebElement myWishListHeader;
	
	@FindBy(xpath="//span[text()=' Back to your account']")
	WebElement backButton;
	

	public YLMyAccountPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	public void clickOrderHistory() {
		orderHistory.click();
	}

	public void clickCreditSlips() {
		creditSlips.click();
	}
	
	public void clickMyAdress() {
		myAddress.click();
	}
	
	public void clickPersonalInfo() {
		personalInfo.click();
	}
	
	public void clickMyWishList() {
		myWishList.click();
	}
	
	public void clickBackButton() {
		backButton.click();
	}
	
	
	
	
	public void checkMyAccountPage(
			String expectedHistory, 
			String expectedCreditInfo,
			String expectedAddressesText,
			String expectedPInfo,
			String expectedMyList
			) {
		//Order History
		this.clickOrderHistory();
		String history = this.infoPanel.getText();
		Assert.assertEquals(history, expectedHistory);
		driver.navigate().back();
		//Credit Info
		this.clickCreditSlips();
		String credit = this.infoPanel.getText();
		Assert.assertEquals(credit, expectedCreditInfo);
		driver.navigate().back();
		// Address block
		this.clickMyAdress();
		String addressText = addressesText.getText();
		Assert.assertEquals(addressText, expectedAddressesText);
		driver.navigate().back();
		// Personal information
		this.clickPersonalInfo();
		String pInfo = personalInfoHeader.getText();
		Assert.assertEquals(pInfo, expectedPInfo);
		driver.navigate().back();
		// My Wish List
		this.clickMyWishList();
		String myList = myWishListHeader.getText();
		Assert.assertEquals(myList, expectedMyList);
		driver.navigate().back();		
		
	}
}
