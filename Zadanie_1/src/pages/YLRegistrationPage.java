package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class YLRegistrationPage {
	
	WebDriver driver;
		
	//Personal information
	@FindBy(id="customer_firstname")
	WebElement customerFirstName;
	
	@FindBy(id="customer_lastname")
	WebElement customerLastName;
	
	@FindBy(id="email")
	WebElement email;
	
	@FindBy(id="passwd")
	WebElement pasword;
	
	//Address information
	@FindBy(id="firstname")
	WebElement addressFirstName;
	
	@FindBy(id="lastname")
	WebElement addressLastName;
	
	@FindBy(id="address1")
	WebElement address;
	
	@FindBy(id="city")
	WebElement city;
	
	@FindBy(id="id_state")
	WebElement state;
	
	@FindBy(id="postcode")
	WebElement postalCode;
	
	@FindBy(id="phone_mobile")
	WebElement phoneNumber;
	
	@FindBy(id="alias")
	WebElement aliasEmail;
	
	//Button
	@FindBy(id="submitAccount")
	WebElement registerButton;
	
	public YLRegistrationPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	public void setCustFName(String firstName) {
		customerFirstName.sendKeys(firstName);
	}
	
	public void setCustLName(String lastName) {
		customerLastName.sendKeys(lastName);
	}
	
	public void setPassword(String password) {
		pasword.sendKeys(password);
	}
	
	public void setAddressFName(String fName) {
		addressFirstName.sendKeys(fName);
	}
	
	public void setAddressLName(String lName) {
		addressLastName.sendKeys(lName);
	}
	
	public void setAddress(String adress) {
		address.sendKeys(adress);
	}

	public void setCity(String addressCity) {
		city.sendKeys(addressCity);
	}
	
	public void setState(String stateOption) {
		Select drpState = new Select(state);
		drpState.selectByVisibleText(stateOption);
	}
	
	public void setPostalCode(String postal) {
		postalCode.sendKeys(postal);
	}
	
	public void setMobileNumber(String mobileNumber) {
		phoneNumber.sendKeys(mobileNumber);
	}
	
	public void ClickRegisterButton() {
		registerButton.click();
	}
	
	
	public void registerYLPage(
			String customerFirstName, 
			String customerLastName, 
			String password, 
			String address,
			String city,
			String state,
			String postalCode,
			String mobileNumber) {
		
		//Sign In
		this.setCustFName(customerFirstName);
		this.setCustLName(customerLastName);
		this.setPassword(password);
		this.setAddress(address);
		this.setCity(city);
		this.setState(state);
		this.setPostalCode(postalCode);
		this.setMobileNumber(mobileNumber);
		this.ClickRegisterButton();		
	}
	
	
	
	
}
