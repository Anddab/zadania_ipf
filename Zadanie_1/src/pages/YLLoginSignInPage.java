package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class YLLoginSignInPage {

	WebDriver driver;

	@FindBy(id="email_create")
	WebElement emailInput;
	
	@FindBy(id="SubmitCreate")
	WebElement createAccountButton;
	
	@FindBy(id="email")
	WebElement login;
	
	@FindBy(id="passwd")
	WebElement password;
	
	@FindBy(id="SubmitLogin")
	WebElement loginButton;

	public YLLoginSignInPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	public void setRegEmail(String Email) {
		emailInput.sendKeys(Email);
	}
	
	public void clickCreateAnAccount() {
		createAccountButton.click();
	}
	
	
	public void setLogEmail(String loginEmail) {
		login.sendKeys(loginEmail);
	}
	
	public void setLogPassword(String passwd) {
		password.sendKeys(passwd);
	}
	
	public void ClickLogButton() {
		loginButton.click();
	}
	
	
	
	public void startSignIn(String email) {
		this.setRegEmail(email);
		this.clickCreateAnAccount();		
	}
	
	public void startLogIn(
			String email,
			String password) {
		this.setLogEmail(email);
		this.setLogPassword(password);
		this.ClickLogButton();
	}
}
