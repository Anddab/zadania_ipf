package test;

import java.util.concurrent.TimeUnit;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import pages.YLRegistrationPage;
import pages.YLHomePage;
import pages.YLLoginSignInPage;
import pages.YLMyAccountPage;

public class Tests {

	WebDriver driver;
	
	YLLoginSignInPage logSignInPage;
	YLHomePage homePage;
	YLRegistrationPage regPage;
	YLMyAccountPage myPage;
	
	String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
	String emailAddress = timeStamp + "@gmail.com";
	
	String fName = "Zenon";
	String lName = "Nowak";
	String pswd = "Haslo1234";
	String address = "potulna 2";
	String city = "London";
	String state = "Alabama";
	String postalCode = "12345";
	String mobileNumber = "123456789";
	
	String expectedHistory = "You have not placed any orders.";
	String expectedCreditInfo = "You have not received any credit slips.";
	String expectedAddressesText = "Your addresses are listed below.";
	String expectedPInfo = "YOUR PERSONAL INFORMATION";
	String expectedMyList = "MY WISHLISTS";
	
	
	@BeforeSuite
	public void setup() {
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get("http://automationpractice.com/index.php");
	}
	
	@AfterSuite
	public void setdown() {
		driver.quit();
	}
	
	@Test(priority=0)
	public void test_signIn() {
		
		homePage = new YLHomePage(driver);
		homePage.clickSignInButton();
		
		logSignInPage = new YLLoginSignInPage(driver);
		logSignInPage.startSignIn(emailAddress);
		
		regPage = new YLRegistrationPage(driver);
		regPage.registerYLPage(fName, lName, pswd, address ,city, state, postalCode, mobileNumber);
		
		String pageTitle = driver.getTitle();
		Assert.assertTrue(pageTitle.contains("My account - My Store"));
	}
	
	@Test(priority=1)
	private void test_logIn() {
		//Logowanie danymi usera stworzonego chwile wczesniej
		homePage = new YLHomePage(driver);
		homePage.clickSignInButton();
		
		logSignInPage = new YLLoginSignInPage(driver);
		logSignInPage.startLogIn(emailAddress, pswd);
		String logpage = driver.getTitle();
		Assert.assertTrue(logpage.contains("My account - My Store"));
	}
	
	@Test(priority=2)
	private void smoke_test_features() {
		//Sprawdzenie działania funkcjonalności konta
		homePage = new YLHomePage(driver);
		homePage.clickSignInButton();
		
		logSignInPage = new YLLoginSignInPage(driver);
		logSignInPage.startLogIn(emailAddress, pswd);
		String logpage = driver.getTitle();
		Assert.assertTrue(logpage.contains("My account - My Store"));
		
		myPage = new YLMyAccountPage(driver);
		myPage.checkMyAccountPage(
				expectedHistory, 
				expectedCreditInfo, 
				expectedAddressesText, 
				expectedPInfo, 
				expectedMyList);
		
		
	}
	
}
